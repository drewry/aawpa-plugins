<?php 
/**
* Plugin Name: Buddypress Standard
* Author: Russell Fair
* Version: 0.0.1
*/

function bpstd_css_it(){
	$pluginurl =  WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__));
	
	wp_enqueue_style('standard-buddypress', $pluginurl . 'buddypress-standard.css', '001', array('standard-css'), 'all' );
}
add_action('wp_print_styles', 'bpstd_css_it');

function bpstd_setup(){
	//what exactly were you planning here rfair?
}
//add_action('after_setup_theme', 'bpstd_setup');


//add_action('get_sidebar', 'bpstd_before_sidebar', 5);
function bpstd_before_sidebar(){
	//closes out the "#main .span8"
	echo bpstd_get_close_wrap_main();
}
//add_action('get_footer', 'test_before_footer', 5);
function test_before_footer(){
	//closes everything else we created
	echo bpstd_get_close_html();
}

/**REGISTRATION**/
// register page
add_action('bp_before_register_page', 'bpstd_open_html');
add_action('bp_after_register_page', 'bpstd_close_html');
// bpstd_open_wrap_main


//activation
add_action('bp_before_activation_page', 'bpstd_open_html');
add_action('bp_after_activation_page', 'bpstd_close_html');

/**MEMBERS**/
// members/index page
add_action('bp_before_directory_members', 'bpstd_open_html');
add_action('bp_after_directory_members', 'bpstd_close_html');
//members/single/home
add_action('bp_before_member_home_content', 'bpstd_open_html');
add_action('bp_after_member_home_content', 'bpstd_close_html');
//members/single/plugins
add_action('bp_before_member_plugin_template', 'bpstd_open_html');
add_action('bp_after_member_plugin_template', 'bpstd_close_html');
//members/single/settings/capabilities
add_action('bp_before_member_settings_template', 'bpstd_open_html');
add_action('bp_after_member_settings_template', 'bpstd_close_html');
//members/single/settings/delete-account
add_action('bp_before_member_settings_template', 'bpstd_open_html');
add_action('bp_after_member_settings_template', 'bpstd_close_html');
//members/single/settings/general
add_action('bp_before_member_settings_template', 'bpstd_open_html');
add_action('bp_after_member_settings_template', 'bpstd_close_html');
//members/single/settings/notifications
add_action('bp_before_member_settings_template', 'bpstd_open_html');
add_action('bp_after_member_settings_template', 'bpstd_close_html');
//members/single/activity/permalink
// add_action('bp_before_member_settings_template', 'bpstd_open_html');
// add_action('bp_after_member_settings_template', 'bpstd_close_html');


/**GROUPS**/
//create
add_action('bp_before_create_group_content_template', 'bpstd_open_html');
add_action('bp_after_create_group_content_template', 'bpstd_close_html');
//groups/index
add_action('bp_before_directory_groups', 'bpstd_open_html');
add_action('bp_after_directory_groups', 'bpstd_close_html');
//groups/single/home
add_action('bp_before_group_home_content', 'bpstd_open_html');
add_action('bp_after_group_home_content', 'bpstd_close_html');
//groups/single/plugins
add_action('bp_before_group_plugin_template', 'bpstd_open_html');
add_action('bp_after_group_plugin_template', 'bpstd_close_html');


/**FORUMS**/
//forums/index
add_action('bp_before_directory_forums', 'bpstd_open_html');
add_action('bp_after_directory_forums_content', 'bpstd_close_html');

/**BLOGS**/
//create
add_action('bp_before_create_blog_content_template', 'bpstd_open_html');
add_action('bp_after_create_blog_content_template', 'bpstd_close_html');
//blogs/index
add_action('bp_before_directory_blogs', 'bpstd_open_html');
add_action('bp_after_directory_blogs', 'bpstd_close_html');

/**ACTIVITY**/
//activity/index
add_action('bp_before_directory_activity', 'bpstd_open_html');
add_action('bp_after_directory_activity', 'bpstd_close_html');


function bpstd_get_open_html(){
	$standard_html_before_bp = sprintf('<div id="wrapper"><div class="container"><div class="row"><!--%s-->', __('add standard theme markup to buddypress pages','bpstd') );

	return apply_filters('bpstd_open_html_markup', $standard_html_before_bp);
}

function bpstd_get_close_html(){
	$standard_html_after_bp = sprintf('</div><!-- .row --></div><!-- .container --></div><!-- #wrapper --><!--%s-->', __('happy now standard?','bpstd') );

	return apply_filters('bpstd_close_html_markup', $standard_html_after_bp);
}

function bpstd_open_html(){
	echo bpstd_get_open_html();
	echo bpstd_get_open_wrap_main();
	echo '<div class="bpstd-extra1 hentry post clearfix"><div class="bpstd-extra2 entry-content clearfix">';
}

function bpstd_close_html(){
	add_action('get_sidebar', 'bpstd_before_sidebar', 5);
	add_action('get_footer', 'test_before_footer', 5);
}

function bpstd_get_open_wrap_main(){
	$standard_open_wrap_main = sprintf('<div id="main" class="span8 clearfix" role="main"><!--%s-->',  __('add standard theme markup to buddypress pages','bpstd') );

	return apply_filters('bpstd_open_wrap_main_markup', $standard_open_wrap_main);
}

function bpstd_get_close_wrap_main(){
	$standard_close_wrap_main = sprintf('</div><!-- #main --><!--%s-->',  __('now on to the sidebar','bpstd'));

	return apply_filters('bpstd_close_wrap_main_markup', $standard_close_wrap_main);
}
