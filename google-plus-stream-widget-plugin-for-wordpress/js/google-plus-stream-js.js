/**
 * User: nembleton
 * 1.0.0
 */

jQuery(document).ready(function($) {
    $('.google_plus_stream_plugin_stream_post').bind({
        mouseenter: function() {
//            alert($(this).attr("id").match(/[0-9]{1,3}$/));
            var id = $(this).attr("id").match(/[0-9]{1,3}$/);
            var itemNewId = "#google_stream_content_hidden-"+id;
//            console.log(itemNewId);
            $(itemNewId).animate({
                height:'toggle',
                width: $(this).css("width")
            }, 100 ).css("display","block");
        },
        mouseleave: function() {
            var id = $(this).attr("id").match(/[0-9]{1,3}$/);
            var itemNewId = "#google_stream_content_hidden-"+id;
//            console.log(itemNewId);
//            $(itemNewId).css("display","none").animate(100);
            $(itemNewId).animate({
                height:'toggle'
            }, 100 );
        }
    });
});
