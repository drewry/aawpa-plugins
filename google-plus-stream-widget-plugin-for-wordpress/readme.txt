=== Google Plus Stream Widget Plugin For WordPress ===
Contributors: theagence.fr
Tags: Google+, Google Plus, Google+ Stream, Stream, Google Widget
Requires at least: 3.0.0
Tested up to: 3.4.0
Stable tag: 1.0.4
Donate link: http://goo.gl/qMtJ9

This plugin allows you to display the thread of your Google+ Stream right within your WordPress blog in just a few clicks.

== Description ==

Google Plus Stream Widget Plugin For WordPress allow you to add a widget that will, every 30 minutes, check an update on your Google+ Stream wall using Google+ APIs ( authorized way from Google ).

As the API calls are limited to 1000 calls per day, we limited to every 30 minutes. Later we will add shorter scheduler notices.
The collector stores the data into a table that will be later called to display the stream into your Widget. 

Read the SETUP carefully to fulfill the requirements. Nothing hard, don't worry.

You can customize the title and the number of posts in the stream to display. We will add much more option later. But the strict minimum is there.

The plugin includes nice JS effects and ability to read the full post at Google+.

Enjoy

== Screenshots ==

1. Google Plus Streams widget with standard design
2. Widget Configuration
3. Extended stream content
4. Stream with fancy design

== Changelog ==

1.0.4 - 2012.07.09 - Added backward compatibility to PHP < 5.3.0 ( DateInterval and DateTime::diff() doesn't exist for )
1.0.3 - 2012.07.08 - Fixed an error with empty() function, + small improvements<br />
1.0.2 - 2012.03.17 - Now the plugin is silent if you haven't configured your API Key/User but have enabled the Widget in the Admin. Doesn't generate an error ( Thanks Jeanne )<br />
1.0.1 - 2012.01.03 - Fixed the plugin name and the readme.txt, added the screenshots<br />
1.0.0 - 2012.01.03 - Initial Release<br />

== Setup ==

1. Download from WordPress plugin extents
2. Activate the plugin, it will install 1 more table in your DB, register the schedulers ( every 30 minutes ) and register a few options
3. Go to "Google+ Stream" page to enter your credentials ( API Key and Google+ ID, see the tutorials to get API Key and Google+ ID )
4. Go to your Widget page and drag&drop at your favourite place, select your titel and number of posts to display and you're good to go

You're good to go.

== Support ==

Visit us at http://goo.gl/qMtJ9
TheAgence.fr is a web agency that supports WordPress by providing Plugins, Widgets, Themes and more.
If you have a problem, send me email at nicolas.embleton@gmail.com with the subject clearly mentioning "googleplusstreamplugin: "

== Know Bugs ==

Some times the picture and the name will disappear with a 400 or 413 errors from Google Services... I'm suspecting some bugs on Google server side but I will investigate more and eventually fix that bug in a later release.

We did tests on all the featured templates from wordpress.org with no bugs. It should be pretty safe.
Also, all the functions used only require the core php with no specific libs. If you see any lack in our requirements, please send me email about it. I'll fix that.

== Misc ==

We will add a lot of cool features to the plugin so, stay tuned.
More designs, more functions, better integration with G+ services and features.

If you want us to design or increase the plugin for you, send me an email and I'll answer you asap.

Thanks and Viva WordPress ;)