<?php
/*
       Plugin name: Google Plus Stream Widget Plugin for WordPress
       Version: 1.0.4
       Description: Display your Google+ Stream within a widget easily and efficiently. Cached to avoid off-quotas and slow load.
       Author: Nicolas Embleton
       Author URI: http://www.theagence.fr
       Plugin URI: http://www.theagence.fr/solutions/wordpress/plugins/google-plus-stream-widget-plugin-for-wordpress
   */

if (!defined('GOOGLE_PLUS_PLUGIN_NAME')) define('GOOGLE_PLUS_PLUGIN_NAME', "Google Plus Stream Plugin For WordPress");
if (!defined('GOOGLE_PLUS_STREAM_PLUGIN_VERSION')) define('GOOGLE_PLUS_STREAM_PLUGIN_VERSION', '1.0.3');
if (!defined('GOOGLE_PLUS_STREAM_PLUGIN_URL')) define('GOOGLE_PLUS_STREAM_PLUGIN_URL', plugin_dir_url(__FILE__));
if (!defined('GOOGLE_PLUS_STREAM_PLUGIN_CALL_URL')) define('GOOGLE_PLUS_STREAM_PLUGIN_CALL_URL',
"https://www.googleapis.com/plus/v1/people/{YOUR_USER_ID}/activities/public?alt=json&pp=1&key={YOUR_API_KEY}");

if (!defined('GOOGLE_PLUS_API_KEYS')) define("GOOGLE_PLUS_API_KEYS", get_option("google_plus_stream_plugin_api_key"));
if (!defined('GOOGLE_PLUS_USER_ID')) define("GOOGLE_PLUS_USER_ID", get_option("google_plus_stream_plugin_user_id"));

//if (!defined('GOOGLE_PLUS_STREAM_PLUGIN_CALL_URL')) define('GOOGLE_PLUS_STREAM_PLUGIN_CALL_URL',
//    "https://www.googleapis.com/plus/v1/people/{YOUR_USER_ID}/activities/public?alt=json&pp=1&key={YOUR_API_KEY}");

// https://www.googleapis.com/plus/v1/people/103866161283605516596/activities/public?alt=json&pp=1&key={YOUR_API_KEY}

function google_plus_check_table() {
    global $wpdb;
    $stream_table_name = $wpdb->prefix . "google_plus_stream";

    if (!$wpdb->get_results("SELECT * FROM `" . $stream_table_name . "` limit 1")) {
        $sql = "CREATE TABLE `" . $stream_table_name . "` (
			`stream_id` INT NOT NULL AUTO_INCREMENT,
			`stream_post_id` VARCHAR(40) UNIQUE NOT NULL,
			`stream_post_date` DATETIME,
			`stream_post_title` VARCHAR(1000),
			`stream_post_url` VARCHAR(500),
			`stream_post_actor` VARCHAR(500),
			`stream_post_actor_profile_url` VARCHAR(500),
			`stream_post_actor_avatar_url` VARCHAR(500),
			`stream_post_object_content` VARCHAR(1000),
			`stream_post_object_attachement_title` VARCHAR(1000),
			`stream_post_object_attachement_content` VARCHAR(1000),
			`stream_post_object_attachement_url` VARCHAR(1000),
			`stream_post_replies_count` INT(5),
			`stream_post_plus_count` INT(5),
			`stream_post_reshares_count` INT(5),
			PRIMARY KEY (`stream_id`)) ENGINE = MyISAM;";

        if(!$wpdb->query($sql)) {
            google_plus_stream_plugin_log("google_plus_check_table; Table creation failed!!!");
        }
    }
}

function my_deactivation()
{
    // Drop table containing streams
    global $wpdb;
    $stream_table_name = $wpdb->prefix . "google_plus_stream";
    $wpdb->query("DROP TABLE `$stream_table_name`");

    // De-register scheduled events
    wp_clear_scheduled_hook('google_plus_stream_plugin_get_content_hourly');
    wp_clear_scheduled_hook('google_plus_stream_plugin_get_content_hourly_shifted_30_minutes');

    // Drop options
    delete_option("google_plus_stream_plugin_api_key");
    delete_option("google_plus_stream_plugin_user_id");
    delete_option("google_plus_stream_plugin_user_avatar");
    delete_option("google_plus_stream_plugin_user_display_name");
    delete_option("google_plus_stream_plugin_user_profile_url");
}

function google_plus_add_plugin() {
    // register activation and deactivation hooks
    register_activation_hook(__FILE__, google_plus_check_table());
    register_deactivation_hook(__FILE__, 'my_deactivation');

    // Add page to the menu, on the left
    add_menu_page("Database", "Google+ Stream", 'administrator', 'google-plus-stream-admin', 'google_plus_plugin_main', plugins_url("images/logo_sml_sml.png", __FILE__));

    // Register the hourly event of gathering streams
    wp_schedule_event(time(), 'hourly', 'google_plus_stream_plugin_get_content_hourly');
    // Register the hourly event of gathering streams + 30 minutes ( so we have 2 scheduled events to gather every 30 minutes )
    wp_schedule_event(time() + (30 * 60), 'hourly', 'google_plus_stream_plugin_get_content_hourly_shifted_30_minutes');

    // TODO: For upgrading, check this version to make sure if necessary to upgrade the database.
    // add_options to the stack
    add_option("google_plus_stream_plugin_version", GOOGLE_PLUS_STREAM_PLUGIN_VERSION);

    add_option("google_plus_stream_plugin_api_key", "YOUR_API_KEY");
    add_option("google_plus_stream_plugin_user_id", "YOUR_USER_ID");

    add_option("google_plus_stream_plugin_user_avatar", plugins_url("images/logo_sml.png", __FILE__));
    add_option("google_plus_stream_plugin_user_display_name", "http://www.theagence.fr");
    add_option("google_plus_stream_plugin_user_profile_url", "http://www.theagence.fr");

    // Register stylesheet for Widget
    wp_register_style('google_plus_stream_stylesheet', plugins_url('/css/google-plus-stream-style.css', __FILE__));
}

function google_plus_plugin_admin_page_init() {
    $myStyleUrl = plugins_url('/css/google-plus-stream-style-admin.css', __FILE__); // Respects SSL, Style.css is relative to the current file
    if (file_exists($myStyleUrl)) {
        wp_register_style('google_plus_stream_admin_stylesheet', $myStyleUrl);
        wp_enqueue_style('google_plus_stream_admin_stylesheet');
    }
}

function google_plus_plugin_get_content($silent = true) {
    $callUrl = str_replace("{YOUR_USER_ID}", GOOGLE_PLUS_USER_ID, GOOGLE_PLUS_STREAM_PLUGIN_CALL_URL);
    $callUrl = str_replace("{YOUR_API_KEY}", GOOGLE_PLUS_API_KEYS, $callUrl);

    if (!$silent) {
        echo $callUrl . "\n\n\n<br/><br/><br/>";
    }

    if(GOOGLE_PLUS_USER_ID == "" || GOOGLE_PLUS_API_KEYS == "") {
        if (!$silent) {
            echo "\n\n\n<br/> ".GOOGLE_PLUS_PLUGIN_NAME." \n<br /><strong>You appear not to have configured your API Key and/or Username properly. Please check in admin panel</strong><br/><br/>";
        }
        return 0;
    }

    // We're using WP API wp_remote_get ( http://codex.wordpress.org/Function_API/wp_remote_get )
    $remote_content = wp_remote_get($callUrl);

    // If has an error in the connection, log and abort
    if(is_wp_error($remote_content)) {
        google_plus_stream_plugin_log("google_plus_plugin_get_content; Impossible to get Google+ API Post Feed ");
        google_plus_stream_plugin_log("google_plus_plugin_get_content; Response code: " .$remote_content['reponse']['code']);
        google_plus_stream_plugin_log("google_plus_plugin_get_content; Response code: " .$remote_content['reponse']['message']);
        return 0;
    }

    $response = $remote_content['body'];

    $json = json_decode($response);

    $i = 0;

    global $wpdb;
    $stream_table_name = $wpdb->prefix . "google_plus_stream";

    //    var_dump($json);
    foreach ($json->items as $item) {
        //        var_dump($item);

        // update the user info at the first script update
        // Not optimized but let's make it easy to start
        if ($i == 0) {
            $user_avatar = $item->actor->image->url;
            $user_display_name = $item->actor->displayName;
            $user_profile_url = $item->actor->url;
            update_option("google_plus_stream_plugin_user_avatar", $user_avatar);
            update_option("google_plus_stream_plugin_user_display_name", $user_display_name);
            update_option("google_plus_stream_plugin_user_profile_url", $user_profile_url);
        }

        $i++;
        // prepare SQL statement
        $stream_post_id = $item->id;
        $stream_post_date = $item->published;
        $stream_post_title = encodeForDBInsert($item->title);
        $stream_post_url = $item->url;
        $stream_post_actor = $item->actor->displayName;
        $stream_post_actor_profile_url = $item->actor->url;
        $stream_post_actor_avatar_url = $item->actor->image->url;
        $stream_post_object_content = encodeForDBInsert($item->object->content);
        $stream_post_object_attachement_title = encodeForDBInsert($item->object->attachments->displayName);
        $stream_post_object_attachement_content = encodeForDBInsert($item->object->attachments->content);
        $stream_post_object_attachement_url = $item->object->attachments->url;
        $stream_post_replies_count = $item->object->replies->totalItems;
        $stream_post_plus_count = $item->object->plusoners->totalItems;
        $stream_post_reshares_count = $item->object->resharers->totalItems;

        $sql = "
            INSERT INTO `$stream_table_name` (`stream_post_id`, `stream_post_date`, `stream_post_title`, `stream_post_url`, `stream_post_actor`,
                                              `stream_post_actor_profile_url`, `stream_post_actor_avatar_url`, `stream_post_object_content`,
                                              `stream_post_object_attachement_title`, `stream_post_object_attachement_content`,
                                              `stream_post_object_attachement_url`, `stream_post_replies_count`, `stream_post_plus_count`, `stream_post_reshares_count` )
            VALUES(\"$stream_post_id\",\"$stream_post_date\",\"$stream_post_title\",\"$stream_post_url\",\"$stream_post_actor\",\"$stream_post_actor_profile_url\",
                  \"$stream_post_actor_avatar_url\",\"$stream_post_object_content\",\"$stream_post_object_attachement_title\",\"$stream_post_object_attachement_content\",
                  \"$stream_post_object_attachement_url\",\"$stream_post_replies_count\",\"$stream_post_plus_count\",\"$stream_post_reshares_count\"
                  );
        ";
        $sql_sanitized = $wpdb->prepare($sql);
        $result = $wpdb->query($sql_sanitized);

        if($result) {
            google_plus_stream_plugin_log("google_plus_plugin_get_content; Insert successful: " .$sql_sanitized . "<br/>");
        } else {
            if(is_wp_error($result)) {
                google_plus_stream_plugin_log("google_plus_plugin_get_content; There was an error inserting the SQL. Most likely the post is already in the database <br />\n Query : ".$sql_sanitized);
            } else {
                google_plus_stream_plugin_log("google_plus_plugin_get_content; No error, but insert did not happened <br />\n SQL: " .$sql_sanitized);
            }
        }

        if (!$silent) {
            if ($result) {
                echo $sql_sanitized . "<br/>";
            } else {
                echo "Post already in the database <br />";
            }
        }
    }
}

function google_plus_plugin_add_enqueue_style () {
    if(is_active_widget('GooglePlusStreamPluginWidget', false, 'google_plus_stream_plugin_widget', false)) {
        wp_enqueue_style( 'google_plus_stream_stylesheet', plugins_url( '/css/google-plus-stream-style.css', __FILE__ ), false, '1.0', 'all' ); // Inside a plugin
        wp_enqueue_script('google_plus_stream_javascript',
            /* WP_PLUGIN_URL . '/someplugin/js/newscript.js', // old way, not SSL compatible */
            plugins_url('/js/google-plus-stream-js.js', __FILE__), // where the this file is in /someplugin/
            array('jquery'), // depends on jQuery
            '1.0' );
        wp_enqueue_script('google_plus_stream_javascript_google_plus', "https://apis.google.com/js/plusone.js");
    }
}

function encodeForDBInsert($field) {
    return htmlspecialchars(str_replace("\"", "\\\"", $field), ENT_QUOTES);
}

function decodeForDBSelect($field) {
    return str_replace(array("&quot;","&#39;"), array("\"", "'"), htmlspecialchars_decode(str_replace("\\\"", "\"", $field), ENT_QUOTES));
}

/** not used, too complex for the current needs */
function http_request(
    $verb = 'GET', /* HTTP Request Method (GET and POST supported) */
    $ip, /* Target IP/Hostname */
    $port = 80, /* Target TCP port */
    $uri = '/', /* Target URI */
    $getdata = array(), /* HTTP GET Data ie. array('var1' => 'val1', 'var2' => 'val2') */
    $postdata = array(), /* HTTP POST Data ie. array('var1' => 'val1', 'var2' => 'val2') */
    $cookie = array(), /* HTTP Cookie Data ie. array('var1' => 'val1', 'var2' => 'val2') */
    $custom_headers = array(), /* Custom HTTP headers ie. array('Referer: http://localhost/ */
    $timeout = 1000, /* Socket timeout in milliseconds */
    $req_hdr = false, /* Include HTTP request headers */
    $res_hdr = false /* Include HTTP response headers */
)
{
    $ret = '';
    $verb = strtoupper($verb);
    $cookie_str = '';
    $getdata_str = count($getdata) ? '?' : '';
    $postdata_str = '';

    foreach ($getdata as $k => $v)
        $getdata_str .= urlencode($k) . '=' . urlencode($v) . '&';

    foreach ($postdata as $k => $v)
        $postdata_str .= urlencode($k) . '=' . urlencode($v) . '&';

    foreach ($cookie as $k => $v)
        $cookie_str .= urlencode($k) . '=' . urlencode($v) . '; ';

    $crlf = "\r\n";
    $req = $verb . ' ' . $uri . $getdata_str . ' HTTP/1.1' . $crlf;
    $req .= 'Host: ' . $ip . $crlf;
    $req .= 'User-Agent: Mozilla/5.0 Firefox/3.6.12' . $crlf;
    $req .= 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' . $crlf;
    $req .= 'Accept-Language: en-us,en;q=0.5' . $crlf;
    $req .= 'Accept-Encoding: deflate' . $crlf;
    $req .= 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7' . $crlf;

    foreach ($custom_headers as $k => $v)
        $req .= $k . ': ' . $v . $crlf;

    if (!empty($cookie_str))
        $req .= 'Cookie: ' . substr($cookie_str, 0, -2) . $crlf;

    if ($verb == 'POST' && !empty($postdata_str)) {
        $postdata_str = substr($postdata_str, 0, -1);
        $req .= 'Content-Type: application/x-www-form-urlencoded' . $crlf;
        $req .= 'Content-Length: ' . strlen($postdata_str) . $crlf . $crlf;
        $req .= $postdata_str;
    }
    else $req .= $crlf;

    if ($req_hdr)
        $ret .= $req;

    if (($fp = @fsockopen($ip, $port, $errno, $errstr)) == false)
        return "Error $errno: $errstr\n";

    stream_set_timeout($fp, 0, $timeout * 1000);

    fputs($fp, $req);
    while ($line = fgets($fp)) $ret .= $line;
    fclose($fp);

    if (!$res_hdr)
        $ret = substr($ret, strpos($ret, "\r\n\r\n") + 4);

    return $ret;
}

function google_plus_plugin_main()
{
    ?>
<div id="google_plus_stream_option_page_main">
    <h2>Google Plus Stream Plugin for WordPress</h2>

    <h3>Welcome to Google Plus Stream Plugin for WordPress </h3>
    <h4>Current installed version: <?php echo GOOGLE_PLUS_STREAM_PLUGIN_VERSION; ?></h4>
    <?php

    $action = "";//isset($_GET['action']) ? $_GET['action'] : "add";

    switch ($action) {

        default:
            //echo "no action";
            break;


    }
    google_plus_stream_show_options_form();
    google_plus_plugin_show_admin_page_footer();

    ?> </div> <?php
}

function google_plus_plugin_show_admin_page_footer()
{
    ?>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<a href="http://www.theagence.fr/solutions/wordpress/plugins/google-plus-stream-widget-plugin-for-wordpress/how-to-get-your-google-api-key"
   target="_blank" name="How to get your API Keys for Google APIs">1. How to get your Google+ API KEY</a><br/>
<a href="http://www.theagence.fr/solutions/wordpress/plugins/google-plus-stream-widget-plugin-for-wordpress/how-to-get-your-google-plus-profile-id"
   target="_blank" name="How to get your google plus profile ID"> 2. How to get your Google+ Profile ID </a><br/>
<br/>
<p style="font-style: italic;">
    For customization or if you want us to realize some of your projects, please feel free to visit our Web Agency at <a href="http://www.theagence.fr/" target="_blank" title="The Agence web agency on the web">TheAgence.fr</a>
</p>
<p style="font-style: italic;">
    Plugin support page and donations ( if you feel in the mood of rewarding us for our work ): <a href="http://www.theagence.fr/solutions/wordpress/plugins/google-plus-stream-widget-plugin-for-wordpress/" target="_blank" title="G+ Stream Plugin Homepage">Google+ Stream Widget Plugin For WordPress Homepage</a>
</p>
<p>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_s-xclick">
    <input type="hidden" name="hosted_button_id" value="TP44VEU26MVHW">
    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
</p>
<?php
    // Get the actual content ( for outputting purpose )
    // Note ( 2012.01.03, Nicolas Embleton, I moved this action to the Widget "Save" action to make sure the data is up to date when you include your widget )
//    google_plus_plugin_get_content();

}

function google_plus_stream_show_options_form() {
    $ok = $_POST['ok'] ? $_POST['ok'] : false;
    $api_key = $_POST['google_plus_stream_plugin_api_key'];
    $user_id = $_POST['google_plus_stream_plugin_user_id'];

    $current_api_key = get_option('google_plus_stream_plugin_api_key');
    $current_user_id = get_option('google_plus_stream_plugin_user_id');

    $api_key_edited = false;
    $user_id_edited = false;

    if ($ok) {
        if ($current_api_key != $api_key) {
            update_option('google_plus_stream_plugin_api_key', $api_key);
            $current_api_key = $api_key;
            $api_key_edited = true;
        }

        if ($current_user_id != $user_id) {
            update_option('google_plus_stream_plugin_user_id', $user_id);
            $current_user_id = $user_id;
            $user_id_edited = true;

            // If we detect UserID change, then empty the cache so we don't mistake different people's streams
            global $wpdb;
            $stream_table_name = $wpdb->prefix . "google_plus_stream";
            $wpdb->query("TRUNCATE TABLE $stream_table_name");
            if(!$wpdb) echo "failure";
        }
    }

    $display = ($api_key_edited || $user_id_edited) ? "block" : "none";
    ?>
<div class="wrap">
    <h2><?php echo GOOGLE_PLUS_PLUGIN_NAME . " (v" . GOOGLE_PLUS_STREAM_PLUGIN_VERSION . ")";  ?></h2>

    <p style="width:100%;height:35px;background-color: #fafad2;border:1px solid green;display:<?php echo $display; ?>;text-align: left;padding:10px;">
        <?php
        if ($user_id_edited) {
            echo "User ID have been updated successfully<br/>";
        }
        if ($api_key_edited) {
            echo "API Key have been updated successfully<br/>";
        }
        ?>
    </p>

    <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
        <input type="hidden" name="action" value="edit">
        <input type="hidden" name="ok" value="true">

        <table class="form-table">
            <thead>
            <th>
                <tr>
                    <td width="400">Value:</td>
                    <td>Please input here:</td>
                </tr>
            </th>
            </thead>
            <tbody>
            <tr valign="top">
                <td><label for="google_plus_stream_plugin_api_key">Enter your API Keys ( from google code, tutorial in
                    the footer )</label></td>
                <td><input type="text" id="google_plus_stream_plugin_api_key" name="google_plus_stream_plugin_api_key"
                           value="<?php echo $current_api_key; ?>"/></td>
            </tr>

            <tr valign="top">
                <td><label for="google_plus_stream_plugin_user_id">Enter your User ID ( from google+, tutorial in the
                    footer )</td>
                <td><input type="text" id="google_plus_stream_plugin_user_id" name="google_plus_stream_plugin_user_id"
                           value="<?php echo $current_user_id; ?>"/>
                </td>
            </tr>
            </tbody>
        </table>

        <p class="submit">
            <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>"/>
        </p>

    </form>
</div>

<?php
    // Saves options:
    // google_plus_stream_plugin_user_id
    // google_plus_stream_plugin_api_key
}

/** Actual Widget */

class GooglePlusStreamPluginWidget extends WP_Widget {
    protected $google_stream_manager;

    function GooglePlusStreamPluginWidget() {
        // widget actual processes
        parent::WP_Widget( /* Base ID */
            'google_plus_stream_plugin_widget', /* Name */
            'GooglePlusStreamPluginWidget', array('description' => 'Displays your Google+ account Streams within a Widget'));
    }

    function form($instance) {
        if ($instance) {
            $title = esc_attr($instance['title']);
            $qty = esc_attr($instance['qty']);
        }
        else {
            $title = __('Google Plus Streams', 'text_domain');
            $qty = __('3', 'text_domain');
        }
        ?>
    <p>
        <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
               name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"/>
        <label for="<?php echo $this->get_field_id('qty'); ?>"><?php _e('Number of posts:'); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('qty'); ?>"
               name="<?php echo $this->get_field_name('qty'); ?>" type="text" value="<?php echo $qty; ?>"/>
    </p>
    <?php
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['qty'] = strip_tags($new_instance['qty']);

        /**
         * In case we're in the first "save" action, let's force to refresh the database by calling the data collector function
         */
        google_plus_plugin_get_content();
        return $instance;
    }

    function widget($args, $instance) {
        $this->google_stream_manager = new GooglePlusStreamManager($instance['qty']);
        $this->google_stream_manager->loadStreams();

        // outputs the content of the widget
        /**
         * Available fields:
        protected $user_avatar;        getAvatar
        protected $user_display_name;  getDisplayName
        protected $user_profile_url;   getProfileURL
        protected $streams;            getStreamAtIndex

        public $stream_post_id;
        public $stream_post_date;
        public $stream_post_title; * use decodeForDBSelect
        public $stream_post_url;
        public $stream_post_actor;
        public $stream_post_actor_profile_url;
        public $stream_post_actor_avatar_url;
        public $stream_post_object_content; * use decodeForDBSelect
        public $stream_post_object_attachement_title; * use decodeForDBSelect
        public $stream_post_object_attachement_content; * use decodeForDBSelect
        public $stream_post_object_attachement_url;
        public $stream_post_replies_count;
        public $stream_post_plus_count;
         */
        extract($args);
        $before_widget = empty($before_widget) ? "" : $before_widget;
        $before_title = empty($before_title) ? "" : $before_title;
        $after_title = empty($after_title) ? "" : $after_title;
        $after_widget = empty($after_widget) ? "" : $after_widget;

        $title = apply_filters('widget_title', $instance['title']);
        echo $before_widget;
        ?>

    <div class="google_plus_stream_plugin_container">
        <div class="google_plus_stream_plugin_header_container">
            <?php if ($title) { echo $before_title . $title . $after_title; } ?>
        </div>
        <div class="google_plus_stream_plugin_profile_container">
            <?php /*
                <g:plus href="https://plus.google.com/<?php echo the_permalink(); ?>" size="badge"></g:plus>
                       */
            ?>
            <a href="<?php echo $this->google_stream_manager->getProfileURL(); ?>" target="_blank" alt="<?php echo $this->google_stream_manager->getDisplayName(); ?>'s Google+ Profile"><img src="<?php echo $this->google_stream_manager->getAvatar(); ?>" alt="<?php echo $this->google_stream_manager->getDisplayName(); ?>'s Google+ Profile Avatar" name="" /></a>
            <span><a href="<?php echo $this->google_stream_manager->getProfileURL(); ?>" target="_blank" alt="<?php echo $this->google_stream_manager->getDisplayName(); ?>'s Google+ Profile"><?php echo $this->google_stream_manager->getDisplayName(); ?></a></span>
        </div>
        <div class="google_plus_stream_plugin_streams_container">
            <?php
            for($i = 0; $i < $this->google_stream_manager->getLength();$i++){
                $stream = $this->google_stream_manager->getStreamAtIndex($i);
                $stream_content = strcmp(substr($stream->stream_post_title, 0, 30), substr($stream->stream_post_object_content,0,30)) == 0 ? $stream->stream_post_object_content : ($stream->stream_post_title ."<br/>" . $stream->stream_post_object_content);

                echo "<div id=\"google_stream_post-$i\" class=\"google_plus_stream_plugin_stream_post\">";
                echo "<span id=\"google_stream_content_title-$i\" class=\"google_plus_stream_plugin_title\"><span class=\"google_plus_stream_plugin_post_date\">" .new Cokidoo_DateTime($stream->stream_post_date) . "</span><br/>" . substr($stream->stream_post_title, 0, 30) ."...</span><br/>";
                //echo;
                echo "<span id=\"google_stream_content_hidden-$i\" class=\"google_plus_stream_plugin_stream_hidden\">" . $stream_content ."</span>" . "<span><a href='" . $stream->stream_post_url ."' target='_blank' alt='Read more of ". $this->google_stream_manager->getDisplayName() ." post at Google+' name='Read more of ". $this->google_stream_manager->getDisplayName() ." post at Google+'>read more...</a></span><br/>" ."<br/>";
                echo "<hr />";
                echo "</div>";
                // TODO: Replace "read more..." by an icon
            }
            ?>
        </div>
    </div>
    <?php echo $after_widget;
    }
}

/**
 * GooglePlusStreamManager
 *
 * This class is designed to contain all the information for the google stream
 * and provide facilitators to extract the streams from the database and build
 * up the objects and arrays.
 *
 * Typical usage is:
 * $var = new GooglePlusStreamManager();
 * $var->loadStreams();
 * // init done
 *
 * $var->getStreamAtIndex(0)->stream_post_title; // Get the first stream title
 */
class GooglePlusStreamManager {
    protected $user_avatar;
    protected $user_display_name;
    protected $user_profile_url;
    protected $streams;
    protected $qty_to_display;

    function GooglePlusStreamManager($qty_to_display) {
        // Gets user options from the option stack
        $this->user_avatar = get_option("google_plus_stream_plugin_user_avatar");
        $this->user_display_name = get_option("google_plus_stream_plugin_user_display_name");
        $this->user_profile_url = get_option("google_plus_stream_plugin_user_profile_url");

        // Contains an array of GooglePlusStreamPost objects
        $streams = array();

        $this->qty_to_display = $qty_to_display;
        if($this->qty_to_display <= 0) { $this->qty_to_display = 5; }
    }

    function loadStreams() {
        global $wpdb;
        $stream_table_name = $wpdb->prefix . "google_plus_stream";

        $sql = "SELECT * FROM `$stream_table_name` order by stream_post_date desc LIMIT " .$this->qty_to_display;
        $myrows = $wpdb->get_results($sql);

        // reset streams in object
        $this->streams = array();

        foreach($myrows as $row) {
            $this->createStreamPost($row);
        }
    }

    // Will create a new stream post from a given resource ( typically a sql row )
    function createStreamPost($db_row) {
        // Create a new object
        $stream = new GooglePlusStreamPost($db_row);
        // And push it inside the array
        array_push($this->streams, $stream);
    }

    function getStreamAtIndex($index) {
        return $this->streams[$index];
    }

    function getAvatar() {
        return $this->user_avatar;
    }

    function getDisplayName() {
        return $this->user_display_name;
    }

    function getProfileURL() {
        return $this->user_profile_url;
    }

    function getLength() {
        return count($this->streams);
    }
}

class GooglePlusStreamPost {
    public $stream_post_id;
    public $stream_post_date;
    public $stream_post_title;
    public $stream_post_url;
    public $stream_post_actor;
    public $stream_post_actor_profile_url;
    public $stream_post_actor_avatar_url;
    public $stream_post_object_content;
    public $stream_post_object_attachement_title;
    public $stream_post_object_attachement_content;
    public $stream_post_object_attachement_url;
    public $stream_post_replies_count;
    public $stream_post_plus_count;

    function GooglePlusStreamPost ($_row = null) {
        // init
        $this->loadFromRow($_row);
    }

    function loadFromRow($_row) {
        if($_row == null) {
            $this->stream_post_id = "";
            $this->stream_post_date = "";
            $this->stream_post_title = "";
            $this->stream_post_url = "";
            $this->stream_post_actor = "";
            $this->stream_post_actor_profile_url = "";
            $this->stream_post_actor_avatar_url = "";
            $this->stream_post_object_content = "";
            $this->stream_post_object_attachement_title = "";
            $this->stream_post_object_attachement_content = "";
            $this->stream_post_object_attachement_url = "";
            $this->stream_post_replies_count = "";
            $this->stream_post_plus_count = "";
        } else {
            $this->stream_post_id = $_row->stream_post_id;
            $this->stream_post_date = $_row->stream_post_date;
            $this->stream_post_title = decodeForDBSelect($_row->stream_post_title);
            $this->stream_post_url = $_row->stream_post_url;
            $this->stream_post_actor = $_row->stream_post_actor;
            $this->stream_post_actor_profile_url = $_row->stream_post_actor_profile_url;
            $this->stream_post_actor_avatar_url = $_row->stream_post_actor_avatar_url;
            $this->stream_post_object_content = decodeForDBSelect($_row->stream_post_object_content);
            $this->stream_post_object_attachement_title = decodeForDBSelect($_row->stream_post_object_attachement_title);
            $this->stream_post_object_attachement_content = decodeForDBSelect($_row->stream_post_object_attachement_content);
            $this->stream_post_object_attachement_url = $_row->stream_post_object_attachement_url;
            $this->stream_post_replies_count = $_row->stream_post_replies_count;
            $this->stream_post_plus_count = $_row->stream_post_plus_count;
        }
    }
}

/** Datetime class from Cokidoo */
class Cokidoo_DateTime extends DateTime {

    protected $strings = array(
        'y' => array('1 year ago', '%d years ago'),
        'm' => array('1 month ago', '%d months ago'),
        'd' => array('1 day ago', '%d days ago'),
        'h' => array('1 hour ago', '%d hours ago'),
        'i' => array('1 minute ago', '%d minutes ago'),
        's' => array('now', '%d secons ago'),
    );

    /**
     * Returns the difference from the current time in the format X time ago
     * @return string
     */
    public function __toString() {
        $now = new DateTime('now');
        $diff = $this->diff($now);

        foreach($this->strings as $key => $value){
            if( ($text = $this->getDiffText($key, $diff)) ){
                return $text;
            }
        }
        return '';
    }

    function diff ($secondDate){
        $firstDateTimeStamp = $this->format("U");
        $secondDateTimeStamp = $secondDate->format("U");
        $rv = ($secondDateTimeStamp - $firstDateTimeStamp);
        if(class_exists("DateInterval")) {
            $di = new DateInterval("PT".$rv ."S");
        } else {
            $di = new _DateInterval($rv);
        }
        return $di;
    }

    /**
     * Try to construct the time diff text with the specified interval key
     * @param string $intervalKey A value of: [y,m,d,h,i,s]
     * @param DateInterval $diff
     * @return string|null
     */
    protected function getDiffText($intervalKey, $diff){
        $pluralKey = 1;
        $value = $diff->$intervalKey;
        if($value > 0){
            if($value < 2){
                $pluralKey = 0;
            }
            return sprintf($this->strings[$intervalKey][$pluralKey], $value);
        }
        return null;
    }
}

if(!class_exists("DateInterval")) {
    Class _DateInterval {
        /* Properties */
        public $y = 0;
        public $m = 0;
        public $d = 0;
        public $h = 0;
        public $i = 0;
        public $s = 0;

        /* Methods */
        public function __construct ( $time_to_convert /** in seconds */) {
            $FULL_YEAR = 60*60*24*365.25;
            $FULL_MONTH = 60*60*24*(365.25/12);
            $FULL_DAY = 60*60*24;
            $FULL_HOUR = 60*60;
            $FULL_MINUTE = 60;
            $FULL_SECOND = 1;

            //        $time_to_convert = 176559;
            $seconds = 0;
            $minutes = 0;
            $hours = 0;
            $days = 0;
            $months = 0;
            $years = 0;

            while($time_to_convert >= $FULL_YEAR) {
                $years ++;
                $time_to_convert = $time_to_convert - $FULL_YEAR;
            }

            while($time_to_convert >= $FULL_MONTH) {
                $months ++;
                $time_to_convert = $time_to_convert - $FULL_MONTH;
            }

            while($time_to_convert >= $FULL_DAY) {
                $days ++;
                $time_to_convert = $time_to_convert - $FULL_DAY;
            }

            while($time_to_convert >= $FULL_HOUR) {
                $hours++;
                $time_to_convert = $time_to_convert - $FULL_HOUR;
            }

            while($time_to_convert >= $FULL_MINUTE) {
                $minutes++;
                $time_to_convert = $time_to_convert - $FULL_MINUTE;
            }

            $seconds = $time_to_convert; // remaining seconds
            $this->y = $years;
            $this->m = $months;
            $this->d = $days;
            $this->h = $hours;
            $this->i = $minutes;
            $this->s = $seconds;
        }
    }
}

function google_plus_stream_plugin_check_scheduler(){
    $now = time();
    $_30_minutes_from_now = $now + 1800;

    if(!wp_next_scheduled( 'google_plus_stream_plugin_get_content_hourly' )) {
        // Register the hourly event of gathering streams
        // Register actions to enable the schedulers to schedule them
        wp_schedule_event(time(), 'hourly', 'google_plus_stream_plugin_get_content_hourly');
        google_plus_stream_plugin_log("Hourly scheduler re-enabled, first event at: " .wp_next_scheduled( 'google_plus_stream_plugin_get_content_hourly' ));
    } else {
        google_plus_stream_plugin_log("Next scheduler planned at : " .date("Y-M-d h:i:s",wp_next_scheduled( 'google_plus_stream_plugin_get_content_hourly' )));
    }

    if(!wp_next_scheduled( 'google_plus_stream_plugin_get_content_hourly_shifted_30_minutes') ) {
        // Register the hourly event of gathering streams + 30 minutes ( so we have 2 scheduled events to gather every 30 minutes )
        wp_schedule_event($_30_minutes_from_now, 'hourly', 'google_plus_stream_plugin_get_content_hourly_shifted_30_minutes');
        google_plus_stream_plugin_log("Hourly+30 scheduler re-enabled, first event at: " .wp_next_scheduled( 'google_plus_stream_plugin_get_content_hourly_shifted_30_minutes'));
    } else {
        google_plus_stream_plugin_log("Next scheduler+30 planned at : " .date("Y-M-d h:i:s",wp_next_scheduled( 'google_plus_stream_plugin_get_content_hourly_shifted_30_minutes' )));
    }
}

function google_plus_stream_plugin_log($msg, $newline = true) {
    if(WP_DEBUG) {
        $file = fopen( plugin_dir_path(__FILE__) ."logs.txt", "a+");
        fwrite($file, $msg . ( $newline ? "<br />\n" : "" ));
        fclose($file);
    }
}

//register_widget('GooglePlusStreamPluginWidget');
// register Foo_Widget widget
add_action( 'widgets_init', create_function( '', 'register_widget("GooglePlusStreamPluginWidget");' ) );

add_action("google_plus_stream_plugin_get_content_hourly", "google_plus_plugin_get_content");
add_action("google_plus_stream_plugin_get_content_hourly_shifted_30_minutes", "google_plus_plugin_get_content");

// De-register scheduled events
/** Uncomment to clear your schedules if you have a lot running */
/** Remember to recomment after you run it at least once or it will break the scheduler */
//wp_clear_scheduled_hook('google_plus_stream_plugin_get_content_hourly');
//wp_clear_scheduled_hook('google_plus_stream_plugin_get_content_hourly_shifted_30_minutes');

add_action("wp", "google_plus_stream_plugin_check_scheduler");
add_action("after_setup_theme", 'google_plus_plugin_add_enqueue_style');

if (is_admin()) { // admin actions
    add_action('admin_menu', 'google_plus_add_plugin');
    add_action('admin_init', 'google_plus_plugin_admin_page_init');
} else {
}

// Log if WP_DEBUG enabled
google_plus_stream_plugin_log("Plugin " .GOOGLE_PLUS_PLUGIN_NAME ." loaded " .date("Y-M-d h:i:s",time()) ."\n");

/** TODOs */
// TODO: Cache the stream manager
// TODO: Create a cached version of the widget
// TODO: Rebuild the cached version after every scheduled event
// TODO: Modify the Widget Class just to load info from the cache

// TODO: Clean cache/truncate DB when detect UserID change
// TODO: Add a button to "force refresh" the content ( clean the datas and regenerate the cache )

// TODO: If necessary, allow multiple Widget with multiple UserID to run at the same time ( meaning that the configuration is by Widget Instance )

// TODO: Roadmap: Improve the stylesheet
// TODO: Roadmap: add small icons when the post has attached link/post/content ( for shared resources )
// TODO: Roadmap: Add ability to run inside a page full width

// TODO: Verify styling within browsers

?>